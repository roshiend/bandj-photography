$( document ).on('turbolinks:load', function() {

  $("#pictureShoot_pictures").on('change', function() {
      //Get count of selected files
      var countFiles = $(this)[0].files.length;
      var imgPath = $(this)[0].value;
      var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
      var image_holder = $("#image-holder");
      image_holder.empty();
      if (extn == "png" || extn == "jpg" || extn == "jpeg") {
        if (typeof(FileReader) != "undefined") {
          //loop for each file selected for uploaded.
          for (var i = 0; i < countFiles; i++) 
          {
            var reader = new FileReader();
            reader.onload = function(e) {
               var division =$("<div class='col-md-4 col-lg-3 col-xl-3'></div>");
               var preview_itm_bx = $("<div class='preview-itm-bx'></div>");
               $(preview_itm_bx).appendTo(division);
               var img_src = $('<img/>').attr({"src": e.target.result, "class": "thumb-image img-fluid"}).appendTo(preview_itm_bx);
                // $("<div class='image-'><img /> </div>", {"src": e.target.result, "class": "thumb-image img-fluid"}).appendTo(image_holder);
              $(division).appendTo(image_holder);
                
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[i]);
          }
        } else {
          alert("This browser does not support FileReader.");
        }
      } else {
        alert("Pls select only images");
      }
    });
 });
