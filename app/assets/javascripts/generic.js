$( document ).ready(function () {
		$(".home-albums-all").slice(0, 1).show();
		if ($(".home-albums-all:hidden").length != 0) {
			$("#load").show();
		}		
		$("#view-more-btn").on('click', function (e) {
			e.preventDefault();
			$(".home-albums-all:hidden").slice(0, 1).slideDown();
			if ($(".home-albums-all:hidden").length == 0) {
				$("#load").fadeOut('slow');
			}
		});
	});