class AlbumsController < ApplicationController

##backend views
  
#before anything verify user loged in?  
   before_action :authenticate_rathne!


  def index
    @album = Album.paginate(:page => params[:page], :per_page => 8)
      respond_to do |format|
        format.html 
        format.js 
      end
  end
  
  def new
     @album = Album.new
  end
  
  def create
    @album = Album.new(album_params)
    @album.save
    if @album.save
      respond_to do |format|
        format.html {redirect_to albums_path puts "created and save"}
        format.js 
      end  
    else
      render 'edit'
      puts "cannot save"
    end    
  end
  
  def show
    @album = Album.find(params[:id])
  end  
  
  def edit
    @album = Album.find(params[:id])
    respond_to do |format|
      format.html 
      format.js 
    end
  end
  
  def update
    @album = Album.find(params[:id])
    if @album.update(album_params)
      respond_to do |format|
        format.html {redirect_to album_path(@album)}
        format.js 
      end
    end
  end
  
  def destroy
    @album = Album.find(params[:id])
    @album.destroy
    redirect_to albums_path
  end  

#delete invidual picture in current album  
  def del_picture 
    #find active storage attachemnt (picture) and delete it from current album'
    @album_child_pic = ActiveStorage::Attachment.find(params[:id])
    @album_child_pic.purge_later
    redirect_back(fallback_location: albums_path)
  end  

  
  private
    def album_params
     params.require(:album).permit(:title,:picture_caption,:shooting_date, :location, pictures:[])
    end  
    
    
end
