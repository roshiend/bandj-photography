class ApplicationController < ActionController::Base
## error handler redirection on 404 and 500 
  rescue_from ArgumentError, :with => :argument_error_rescue 
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found 
  
##route error
  rescue_from ActionController::RoutingError do |exception|
    catch_404
  end
  
  
  
  
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
    update_attrs = [:username, :email, :password, :password_confirmation, :remember_me, :portfolio_image]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: update_attrs
  end
  
  
  def after_sign_out_path_for(resource_or_scope)
    dashboard_path
  end
  
  def argument_error_rescue
    redirect_to root_path
  end 
    
  def record_not_found
    redirect_to root_path
  end
  
  def catch_404
    redirect_to root_path
  end
  
  
  
  
  
end
