class EditorsController < ApplicationController
#admin view
  before_action :authenticate_rathne!
  
#admin dashboard view
  def dashboard
    @edit_landings = LandingImage.paginate(:page => params[:page], :per_page => 4)
    @edit_albums = Album.paginate(:page => params[:page], :per_page => 8)
  end
end
