class ErrorsController < ApplicationController
    protect_from_forgery with: :null_session

  def not_found
    redirect_to root_path
  end

  def internal_server_error
    redirect_to  root_path
  end
  
  def catch_404
   redirect_to root_path
   
  end
  
  
end