class LandingImagesController < ApplicationController
#before anything verify user loged in?  
  before_action :authenticate_rathne!
  
  def index
    @landing_image = LandingImage.paginate(:page => params[:page], :per_page => 2)
  end  
  
  def new
    @landing_image = LandingImage.new
  end  
  
  def create
    @landing_image = LandingImage.new(landing_image_params)
    if @landing_image.save
      redirect_to landing_images_path 
    else
      render 'new'
    end
  end
  
  def show
     @landing_image = LandingImage.find(params[:id])
  end 
  
  def edit
    @landing_image = LandingImage.find(params[:id])
    respond_to do |format|
        format.html 
        format.js 
    end
  end
  
  def destroy
    @landing_image = LandingImage.find(params[:id])
    @landing_image.destroy
    redirect_to landing_images_path
  end
  
  def update
    @landing_image = LandingImage.find(params[:id])
    if @landing_image.update_attributes(landing_image_params)
     redirect_to landing_images_path
     flash[:success] = "Image updated"
    else
      render 'edit'
    end
  end  
  
  private
    def landing_image_params
      params.require(:landing_image).permit(:photo,:header,:caption)
    end 
    
    
end
