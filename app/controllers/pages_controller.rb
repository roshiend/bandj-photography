class PagesController < ApplicationController
  
#frontend 
  def home
    @LandingImages = LandingImage.all.order("ID")
  end
  
  def photoshoots
   @albums = Album.all.order("created_at desc")
  end  
  
  def about
  end  
  
  def contact
  end  
end
