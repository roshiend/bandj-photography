class PasswordsController < Devise::PasswordsController
#custom devise actions
#redirect to same same location if login failed 
 
  def create
    self.resource = resource_class.send_reset_password_instructions(params[resource_name])

    if resource.errors.empty?
      set_flash_message(:notice, :send_instructions) if is_navigational_format?
      respond_with resource, :location => new_session_path(resource_name)
    else

    # Redirect to custom page instead of displaying errors
    redirect_to root_path
      redirect_to new_rathne_password_path , alert: "Email could not found"
    end
  end
end