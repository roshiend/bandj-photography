class Album < ApplicationRecord

# each album has many pictures  
  has_many_attached :pictures
 

#picture sizes
  def thumbnail input
    return self.pictures[input].variant(combine_options: {
      auto_orient: true,
      gravity: "south",
      resize: "300x300^",
      crop: "300x600+0+0"
      }).processed
  end   
   
  def big input
    return self.pictures[input].variant(combine_options: {
    gravity: "center",
    }).processed
  end   
  
end
