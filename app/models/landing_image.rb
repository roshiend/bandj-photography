class LandingImage < ApplicationRecord
#landing Image has one photo
  has_one_attached :photo

#landing Image photo sizes
  def medium
    return self.photo.variant(resize: '400x600!').processed
  end 
  
  def thumb
    return self.photo.variant(resize: '300x300!').processed
  end 
end


