Rails.application.routes.draw do
  
 
 devise_for :rathnes, :controllers => {:passwords => "passwords" , :registrations => "registrations"} 
 resources :landing_images
  resources :albums do
    member do
      delete :del_picture
    end
  end  
  
  root "pages#home"
  get "/dashboard" => "editors#dashboard" ,as: :dashboard
  get "/account" => "editors#account_management", as: :account_management
  
  get "/photoshoots" => "pages#photoshoots" ,as: :photoshoots
  get "/about" => "pages#about" ,as: :about
  get "/contact" => "pages#contact" ,as: :contact
  
  resources :pages, only:[:home]
   

 
end
