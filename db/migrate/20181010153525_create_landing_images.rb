class CreateLandingImages < ActiveRecord::Migration[5.2]
  def change
    create_table :landing_images do |t|
        
      t.string :header
      t.string :caption
      
      t.timestamps
    end
  end
end
