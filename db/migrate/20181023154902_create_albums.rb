class CreateAlbums < ActiveRecord::Migration[5.2]
  def change
    create_table :albums do |t|
      t.string :title # album title
      t.string :picture_caption # album description
      t.string :shooting_date # when did you shoot
      t.string :location #where did you shoot?
      t.timestamps
    end
  end
end
